﻿using System.Collections;
using UnityEngine;

namespace TSP.MergeTown.Game {
	public sealed class LevelInfo {
		public readonly int Index;
		public readonly int Score;

		public LevelInfo(int index, int score) {
			Index = index;
			Score = score;
		}
	}
}