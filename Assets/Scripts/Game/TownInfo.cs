﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TSP.MergeTown.Game {
	public sealed class TownInfo {
		public readonly int    Level;
		public readonly string Name;
		public readonly int    Price;
		public readonly int    Reward;

		public TownInfo(int level, string name, int price, int reward) {
			Level = level;
			Name = name;
			Price = price;
			Reward = reward;
		}
	}
}