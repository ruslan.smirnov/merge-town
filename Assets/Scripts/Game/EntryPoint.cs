﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TSP.MergeTown.Core;
using TSP.MergeTown.Game.Windows;

namespace TSP.MergeTown.Game {
	public sealed class EntryPoint : MonoBehaviour {
		[SerializeField] private GameObject _rootCanvas = null;

		private GameData       _gameData;
		private ProfileManager _profileManager;
		private Store          _store;
		private WindowsManager _windowsManager;

		private void OnSceneGameLoaded(Scene scene, LoadSceneMode loadMode) {
			SceneManager.SetActiveScene(scene);
			SceneManager.sceneLoaded -= OnSceneGameLoaded;
		}

		private void Awake() {
			_gameData = new GameData();
			_profileManager = new ProfileManager();
			_store = new Store();
			_windowsManager = new WindowsManager();
		}

		private void Start() {
			_store.Setup();
			_profileManager.Setup();
			_windowsManager.SetupRootCanvas(_rootCanvas);

			SceneManager.sceneLoaded += OnSceneGameLoaded;
			SceneManager.LoadSceneAsync("Game", LoadSceneMode.Additive);
		}
	}
}