﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace TSP.MergeTown.Game {
	public sealed class Clicker : MonoBehaviour {
		private const float  TIMER_VALUE  = 10;
		private const string FORMAT_TIMER = "{0:0}";

		public Action TimeLeft;

		[SerializeField] private Button _button = null;

		[SerializeField] private TextMeshProUGUI _timeText = null;

		private float _timer;
		private bool  _isRuned = false;

		public void Run() {
			_timer = TIMER_VALUE;
			_isRuned = true;
		}

		private void OnClickButton() {
			_timer = _timer - (_timer - Mathf.FloorToInt(_timer));
		}

		private void Awake() {
			_button.onClick.AddListener(OnClickButton);
		}

		private void Update() {
			if ( _isRuned ) {
				_timer -= Time.deltaTime;

				_timeText.text = string.Format(FORMAT_TIMER, _timer);

				if ( _timer <= 0 ) {
					TimeLeft?.Invoke();
					Run();
				}
			}
		}
	}
}