﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TSP.MergeTown.Core;
using TSP.MergeTown.Game.Windows;

namespace TSP.MergeTown.Game {
	public sealed class GameController : MonoBehaviour {
		public static GameController Instance;

		public GameObject Canvas { get { return _canvas; } }

		public Sprite StoreElementImageDummy { get { return _storeElementImageDummy; } }

		[SerializeField] private PlayField _playField = null;

		[SerializeField] private Clicker _clicker = null;

		[SerializeField] private List<Sprite> _townSprites = null;

		[SerializeField] private GameObject _canvas = null;

		[SerializeField] private Sprite _storeElementImageDummy = null;

		[SerializeField] private Button _storeButton = null;

		public Sprite GetTownSprite(TownInfo townInfo) {
			return townInfo != null ? _townSprites[townInfo.Level - 1] : null;
		}

		public Sprite GetTownSprite(int level) {
			return 0 < level && level < _townSprites.Count
				? _townSprites[level - 1]
				: null;
		}

		private void OnClickStoreButton() {
			Store.Instance.ShowStore();
		}

		private void OnClickBuyTownButton(int index) {
			if ( !_playField.IsPlayFieldFilled() ) {
				_playField.SpawnTownBought(index);

				// вызываем списывание баланса здесь
			}
		}

		private void OnGameOver() {
			WindowsManager.Instance.Show<GameOverWindow>();
		}

		private void Awake() {
			Instance = this;

			ProfileManager.Instance.GameOver += OnGameOver;
			Store.Instance.BuyTownButtonClicked += OnClickBuyTownButton;
			_playField.PlatformMerged += ProfileManager.Instance.IncTownLevel;

			_storeButton.onClick.AddListener(OnClickStoreButton);
		}

		private void Start() {
			_clicker.TimeLeft += _playField.SpawnTown;
			_clicker.Run();

			_playField.Setup();
		}

		private void OnDestroy() {
			ProfileManager.Instance.GameOver -= OnGameOver;
			Store.Instance.BuyTownButtonClicked -= OnClickBuyTownButton;
			_playField.PlatformMerged -= ProfileManager.Instance.IncTownLevel;

			_storeButton.onClick.RemoveAllListeners();
			Instance = null;
		}
	}
}