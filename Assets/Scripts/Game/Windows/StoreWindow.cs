﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TSP.MergeTown.Core;

namespace TSP.MergeTown.Game.Windows {
	public sealed class StoreWindow : WindowAbstract {
		[SerializeField] private TownStoreElement _storeElementTemplate = null;

		[SerializeField] private GameObject _storeContent = null;

		[SerializeField] private Button _closeButton = null;

		private readonly List<TownStoreElement> _storeElements = new List<TownStoreElement>();

		public override void Close() {
			base.Close();

			_closeButton.onClick.RemoveAllListeners();

			foreach ( TownStoreElement item in _storeElements ) {
				item.Clear();
			}

			Destroy(gameObject);
		}

		public override void Setup() {
			base.Setup();

			var store = Store.Instance;
			var profile = ProfileManager.Instance;
			var gameData = GameData.Instance;
			var towns = gameData.Towns;

			_closeButton.onClick.AddListener(Close);

			if ( profile.CurrentTownLevel == gameData.GetMinLevel() ) {
				_storeElementTemplate.SetupAsClosed(0);
				_storeElements.Add(_storeElementTemplate);
			}
			else {
				_storeElementTemplate.Setup(towns[0], 0);
				_storeElements.Add(_storeElementTemplate);

				for ( int index = 1; index < towns.Count - 1; index++ ) {
					var element = Instantiate(_storeElementTemplate);
					element.transform.SetParent(_storeContent.transform, false);

					if ( index < profile.CurrentTownLevel - 1 ) {
						element.Setup(towns[index], index);
						_storeElements.Add(element);
					}
					else {
						element.SetupAsClosed(index);
						_storeElements.Add(element);

						break;
					}
				}
			}

			var storeElementTemplateRectTranform = _storeElementTemplate.GetComponent<RectTransform>();
			var storeContentVerticalLayoutGroup = _storeContent.GetComponent<VerticalLayoutGroup>();

			var storeContentRectTransform = _storeContent.GetComponent<RectTransform>();
			storeContentRectTransform.sizeDelta = new Vector2(
				storeContentRectTransform.sizeDelta.x,
				(storeElementTemplateRectTranform.sizeDelta.y + storeContentVerticalLayoutGroup.spacing) * towns.Count);
		}
	}
}