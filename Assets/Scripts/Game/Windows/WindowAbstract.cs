﻿using UnityEngine;

namespace TSP.MergeTown.Game.Windows {
	public abstract class WindowAbstract : MonoBehaviour {
		[SerializeField] protected GameObject _content = null;

		public virtual void Show() {
			_content.SetActive(true);
		}

		public virtual void Close() {
			_content.SetActive(false);
			Destroy(gameObject);
		}

		public virtual void Setup() {
		}
	}
}