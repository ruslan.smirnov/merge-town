﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using TSP.MergeTown.Core;

namespace TSP.MergeTown.Game.Windows {
	public sealed class NoticeOpenTownWindow : WindowAbstract {
		[SerializeField] private Button _closeButton = null;

		[SerializeField] private TextMeshProUGUI _titleText = null;

		[SerializeField] private Image _image = null;

		public override void Setup() {
			base.Setup();

			_closeButton.onClick.AddListener(Close);

			var gc = GameController.Instance;
			var profile = ProfileManager.Instance;
			var gameData = GameData.Instance;

			_image.sprite = gc.GetTownSprite(profile.CurrentTownLevel);
			_titleText.text = gameData.Towns[profile.CurrentTownLevel - 1].Name;
		}

		public override void Close() {
			base.Close();

			_closeButton.onClick.RemoveAllListeners();

			Destroy(gameObject);
		}
	}
}