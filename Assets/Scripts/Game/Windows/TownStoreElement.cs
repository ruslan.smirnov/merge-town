﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using TSP.MergeTown.Core;

namespace TSP.MergeTown.Game.Windows {
	public sealed class TownStoreElement : MonoBehaviour {
		public Image           TownImage;
		public TextMeshProUGUI TitleText;
		public Button          BuyButton;
		public TextMeshProUGUI PriceText;

		private int _index;

		public void Setup(TownInfo townInfo, int index) {
			_index = index;

			TownImage.sprite = GameController.Instance.GetTownSprite(townInfo);
			TitleText.text = townInfo.Name;
			PriceText.text = string.Format("{0}", townInfo.Price);
			BuyButton.onClick.AddListener(OnClickBuyButton);
		}

		public void SetupAsClosed(int index) {
			_index = index;

			TownImage.sprite = GameController.Instance.StoreElementImageDummy;
			TitleText.text = "Closed";
			BuyButton.gameObject.SetActive(false);
		}

		public void Clear() {
			BuyButton.onClick.RemoveAllListeners();
		}

		private void OnClickBuyButton() {
			Store.Instance.BuyTown(_index);
		}
	}
}