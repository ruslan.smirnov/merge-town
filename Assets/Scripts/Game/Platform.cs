﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace TSP.MergeTown.Game {
	public sealed class Platform : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
		private PlayField _playField = null;

		[SerializeField] private Image _townImage = null;

		[SerializeField] private Image _mergeImage = null;

		public TownInfo TownInfo { get; private set; }

		public bool IsEmpty { get { return TownInfo == null; } }

		public bool IsDraggable { get; private set; }

		public bool IsMergeable { get { return _mergeImage.enabled; } }

		private void ShowImage() {
			if ( !IsEmpty ) {
				_townImage.enabled = true;
			}
		}

		private void HideImage() {
			_townImage.enabled = false;
		}

		public void ShowMergeable() {
			_mergeImage.enabled = true;
		}

		public void HideMergeable() {
			_mergeImage.enabled = false;
		}

		public void Free() {
			HideMergeable();
			HideImage();

			TownInfo = null;
		}

		public void SetupPlatform(PlayField playField, TownInfo townInfo) {
			_playField = playField;
			TownInfo = townInfo;
			Sprite townSprite = GameController.Instance.GetTownSprite(TownInfo);
			_townImage.sprite = townSprite;

			ShowImage();
		}

		public void SetupPlatform(TownInfo townInfo) {
			TownInfo = townInfo;
			Sprite townSprite = GameController.Instance.GetTownSprite(TownInfo);
			_townImage.sprite = townSprite;

			ShowImage();
		}

		public void OnBeginDrag(PointerEventData eventData) {
			if ( IsEmpty ) {
				return;
			}

			IsDraggable = true;
			HideImage();

			_playField.ShowPlatformsMergeable(this);
		}

		public void OnDrag(PointerEventData eventData) {
			if ( IsEmpty ) {
				return;
			}

			_playField.SetupTownImageFake(eventData.position, true);
		}

		public void OnEndDrag(PointerEventData eventData) {
			if ( IsEmpty ) {
				return;
			}

			IsDraggable = false;
			_playField.HidePlatformsMergeable();
			ShowImage();

			GameObject gameObjectRaycasted = eventData.pointerCurrentRaycast.gameObject;

			if ( gameObjectRaycasted != null ) {
				Platform target = gameObjectRaycasted.GetComponent<Platform>();

				if ( target != null ) {
					if ( _playField.TryMergePlatform(this, target) ) {
					}
					else if ( _playField.TrySwapPlatform(this, target) ) {
					}
				}
			}
		}

		private void Awake() {
			HideImage();
			HideMergeable();

			IsDraggable = false;
		}
	}
}