﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TSP.MergeTown.Core;

namespace TSP.MergeTown.Game {
	public sealed class PlayField : MonoBehaviour {
		public Action<int> PlatformMerged;

		[SerializeField] private List<Platform> _platforms = null;

		[SerializeField] private Image _townImageFake = null;

		private List<Platform> GetPlatformsMergeable(TownInfo townInfo) {
			var list = new List<Platform>();

			if ( townInfo != null ) {
				var query = _platforms.FindAll(item =>
					item.TownInfo == null
					|| item.TownInfo.Level == townInfo.Level);

				list.AddRange(query);
			}

			return list;
		}

		public bool TryMergePlatform(Platform current, Platform target) {
			if ( target == null || current == target ) {
				return false;
			}

			TownInfo currentInfo = current.TownInfo;
			TownInfo targetInfo = target.TownInfo;

			if ( targetInfo == null ) {
				return false;
			}

			if ( currentInfo.Level != targetInfo.Level ) {
				return false;
			}

			TownInfo nextInfo = GameData.Instance.GetNextTownInfo(currentInfo);

			target.SetupPlatform(nextInfo);
			current.Free();

			PlatformMerged?.Invoke(currentInfo.Level);

			return true;
		}

		public bool TrySwapPlatform(Platform current, Platform target) {
			if ( target == null || current == target ) {
				return false;
			}

			TownInfo currentInfo = current.TownInfo;
			TownInfo targetInfo = target.TownInfo;

			if ( targetInfo != null ) {
				return false;
			}

			target.SetupPlatform(currentInfo);
			current.Free();

			return true;
		}

		public void ShowPlatformsMergeable(Platform current) {
			var platforms = GetPlatformsMergeable(current.TownInfo);

			foreach ( var item in platforms ) {
				item.ShowMergeable();
			}

			Sprite sprite = GameController.Instance.GetTownSprite(current.TownInfo);
			_townImageFake.sprite = sprite;
			_townImageFake.enabled = true;
		}

		public void HidePlatformsMergeable() {
			foreach ( var item in _platforms ) {
				if ( item.IsMergeable ) {
					item.HideMergeable();
				}
			}

			SetupTownImageFake(new Vector2(-1000, -1000), false);
		}

		public void SetupTownImageFake(Vector2 position, bool isEnable) {
			RectTransform rectTransform = _townImageFake.gameObject.GetComponent<RectTransform>();
			rectTransform.position = position;
			_townImageFake.enabled = isEnable;
		}

		public bool IsPlayFieldFilled() {
			return GetPlatformEmpty() == null;
		}

		public Platform GetPlatformEmpty() {
			return _platforms.Find(item => item.TownInfo == null);
		}

		public bool SpawnTownBought(int townIndex) {
			Platform platform = GetPlatformEmpty();

			if ( platform == null ) {
				Debug.LogFormat("Empty platform is not found");
				return false;
			}

			var town = GameData.Instance.Towns;

			platform.SetupPlatform(town[townIndex]);

			return true;
		}

		public void SpawnTown() {
			var platform = GetPlatformEmpty();

			if ( platform == null ) {
				return;
			}

			platform.SetupPlatform(GameData.Instance.Towns[0]);
		}

		public void Setup() {
			TownInfo initInfo = GameData.Instance.Towns[0];

			foreach ( var item in _platforms ) {
				item.SetupPlatform(this, initInfo);
			}

			SetupTownImageFake(new Vector2(-1000, -1000), false);
		}
	}
}