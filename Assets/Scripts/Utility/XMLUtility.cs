﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

namespace TSP.MergeTown.Utility {
	public sealed class XMLUtility {
		public static XmlDocument LoadFromResources(string path, bool ignoreComments = true) {
			TextAsset textAsset = Resources.Load<TextAsset>(path);
			if ( textAsset == null ) {
				return null;
			}

			return LoadFromText(textAsset.text, ignoreComments);
		}

		public static XmlDocument LoadFromText(string text, bool ignoreComments = true) {
			using ( var stringReader = new StringReader(text) ) {
				var xmlReaderSettings = new XmlReaderSettings {
					IgnoreComments = ignoreComments,
				};
				using ( var xmlReader = XmlReader.Create(stringReader, xmlReaderSettings) ) {
					try {
						var xmlDocument = new XmlDocument();
						xmlDocument.Load(xmlReader);
						return xmlDocument;
					}
					catch ( Exception e ) {
						Debug.LogErrorFormat("XMLUtility: load exception '{0}'", e);
						return null;
					}
				}
			}
		}

		public static void Save(XmlDocument xmlDocument, string path) {
			var xmlWriterSettings = new XmlWriterSettings {
				Indent = true
			};
			using ( var xmlWriter = XmlWriter.Create(path, xmlWriterSettings) ) {
				try {
					xmlDocument.Save(xmlWriter);
				}
				catch ( Exception e ) {
					Debug.LogErrorFormat("XMLUtility: save exception '{0}'", e);
				}
			}
		}
	}
}