﻿using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using TSP.MergeTown.Utility;
using TSP.MergeTown.Game;

namespace TSP.MergeTown.Core {
	public sealed class GameData : DataManager<GameData> {
		public const string PATH_CONFIG = "Configs/GameData";

		public List<TownInfo> Towns { get; private set; }
		public List<LevelInfo> Levels { get; private set; }

		private XmlDocument _document;
		private XmlNode     _root;

		public GameData() {
			if ( TryLoad() ) {
				LoadTownsInfo();
				LoadLevelsInfo();
			}
		}

		public bool TryLoad() {
			_document = XMLUtility.LoadFromResources(PATH_CONFIG);

			if ( _document == null ) {
				Debug.LogFormat("File not found: {0}", PATH_CONFIG);
				return false;
			}

			_root = _document.SelectSingleNode("root");

			return true;
		}

		public void LoadTownsInfo() {
			XmlNode entityRoot = _root.SelectSingleNode("towns");
			XmlNodeList entityNodeList = entityRoot.SelectNodes("town");

			Towns = new List<TownInfo>();

			foreach ( XmlNode entityNode in entityNodeList ) {
				XmlAttribute attributeLevel = entityNode.Attributes["level"];
				XmlAttribute attributeName = entityNode.Attributes["name"];
				XmlAttribute attributePrice = entityNode.Attributes["price"];
				XmlAttribute attributeReward = entityNode.Attributes["reward"];

				int level = int.Parse(attributeLevel.Value);
				string name = attributeName.Value;
				int price = int.Parse(attributePrice.Value);
				int reward = int.Parse(attributePrice.Value);

				Towns.Add(new TownInfo(level, name, price, reward));
			}
		}

		public void LoadLevelsInfo() {
			XmlNode entityRoot = _root.SelectSingleNode("levels");
			XmlNodeList entityNodeList = entityRoot.SelectNodes("level");

			Levels = new List<LevelInfo>();

			foreach ( XmlNode entityNode in entityNodeList ) {
				XmlAttribute attributeIndex = entityNode.Attributes["index"];
				XmlAttribute attributeScore = entityNode.Attributes["score"];

				int level = int.Parse(attributeIndex.Value);
				int score = int.Parse(attributeScore.Value);

				Levels.Add(new LevelInfo(level, score));
			}
		}

		public int GetMaxLevel() {
			return Towns[Towns.Count - 1].Level;
		}

		public int GetMinLevel() {
			return Towns[0].Level;
		}

		public TownInfo GetNextTownInfo(TownInfo currentTownInfo) {
			return currentTownInfo != null ? GameData.Instance.Towns[currentTownInfo.Level] : null;
		}
	}
}