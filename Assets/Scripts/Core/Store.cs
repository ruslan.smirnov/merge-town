﻿using System;
using TSP.MergeTown.Game.Windows;

namespace TSP.MergeTown.Core {
	public sealed class Store : DataManager<Store> {
		private StoreWindow _storeWindow = null;

		public Action<int> BuyTownButtonClicked;

		public Store() {
		}

		public void Setup() {
		}

		public void ShowStore() {
			if ( _storeWindow != null ) {
				return;
			}

			_storeWindow = WindowsManager.Instance.Show<StoreWindow>();
		}

		public void BuyTown(int buttonIndex) {
			BuyTownButtonClicked?.Invoke(buttonIndex);
		}
	}
}