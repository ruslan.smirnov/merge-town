﻿using System.Collections;
using UnityEngine;

namespace TSP.MergeTown.Core {
	public abstract class DataManager {
	}

	public class DataManager<T> : DataManager where T : DataManager<T> {
		public static T Instance { get; private set; }

		public DataManager() : base() {
			if ( Instance != null ) {
				throw new UnityException(string.Format(
					"DataManager{0} already created!",
					typeof(T).Name));
			}

			Instance = this as T;
		}
	}
}