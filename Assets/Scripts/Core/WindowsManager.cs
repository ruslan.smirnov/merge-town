﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
using TSP.MergeTown.Game.Windows;

namespace TSP.MergeTown.Core {
	public sealed class WindowsManager : DataManager<WindowsManager> {
		private GameObject _root;

		private readonly Dictionary<Type, string> _windows = new Dictionary<Type, string>() {
			{typeof(GameOverWindow), "Prefabs/Windows/GameOverWindow"},
			{typeof(NoticeOpenTownWindow), "Prefabs/Windows/NoticeOpenTownWindow"},
			{typeof(StoreWindow), "Prefabs/Windows/StoreWindow"},
		};

		public WindowsManager() {
		}

		public void SetupRootCanvas(GameObject gameObject) {
			_root = gameObject;
		}

		public T Show<T>() where T : WindowAbstract {
			string path = string.Empty;
			_windows.TryGetValue(typeof(T), out path);

			var prefab = Resources.Load<GameObject>(path);
			var gameObject = Object.Instantiate(prefab);
			var window = gameObject.GetComponent<T>();

			window.transform.SetParent(_root.transform, false);
			window.Setup();
			window.Show();

			return window;
		}
	}
}