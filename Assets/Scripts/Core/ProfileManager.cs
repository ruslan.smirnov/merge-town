﻿using System;
using UnityEngine;
using TSP.MergeTown.Game.Windows;

namespace TSP.MergeTown.Core {
	public sealed class ProfileManager : DataManager<ProfileManager> {
		public PlayerInfo PlayerInfo { get; private set; }

		public int CurrentTownLevel { get; private set; }

		public Action GameOver;

		public ProfileManager() {
		}

		public void Setup() {
			var gameData = GameData.Instance;

			PlayerInfo = new PlayerInfo();

			CurrentTownLevel = gameData.GetMinLevel();

			Debug.LogFormat("Levels. min {0} max {1}", gameData.GetMinLevel(), gameData.GetMaxLevel());
		}

		public void Load() {
		}

		public void Save() {
		}

		public void IncTownLevel(int level) {
			if ( level < CurrentTownLevel ) {
				return;
			}

			CurrentTownLevel++;

			Debug.LogFormat("Level up: {0}", CurrentTownLevel);

			if ( IsGameOver() ) {
				GameOver?.Invoke();
			}
			else {
				WindowsManager.Instance.Show<NoticeOpenTownWindow>();
			}
		}

		public bool IsGameOver() {
			return CurrentTownLevel == GameData.Instance.GetMaxLevel();
		}
	}
}