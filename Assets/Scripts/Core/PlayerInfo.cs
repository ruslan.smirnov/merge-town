﻿using System.Collections;
using UnityEngine;

namespace TSP.MergeTown.Core {
	public sealed class PlayerInfo {
		public int Coints { get; private set; }
		public int PlatformLevel { get; private set; }
		public int TownLevel { get; private set; }
	}
}